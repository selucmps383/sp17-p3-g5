# README #

Simple tutorial to get the project running.

### What is this repository for? ###

Angular 2 Project

### How do I get set up? ###

1. Make sure Node is installed (I used version 6.10)
2. Pull project
3. Open node command prompt
4. Change directory to the SRC folder within the project
    EX: cd C:\Users\{UserName}\Documents\sp17-p3-g5\angular-cli-master\sp17-p3-g5\src
5. Run command - ng serve
     -Takes a few moments to build project.
6. The project should build and will host in http://localhost:4200/
7. Open your web browser and go to http://localhost:4200/
Page will load


### Contribution guidelines ###

*DO WORK ;)