import { Sp17P3G5Page } from './app.po';

describe('sp17-p3-g5 App', () => {
  let page: Sp17P3G5Page;

  beforeEach(() => {
    page = new Sp17P3G5Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
